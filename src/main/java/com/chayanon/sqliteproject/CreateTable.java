/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.chayanon.sqliteproject;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author W I N 1 0
 */
public class CreateTable {

    public static void main(String[] args) {
        Connection conn = null;
        String dbName = "user.db";
        Statement stmt=null;

        try {
            Class.forName("org.sqlite.JDBC");
            conn = DriverManager.getConnection("jdbc:sqlite:" + dbName);
            stmt = conn.createStatement();
            String sql="CREATE TABLE COMPANY"+
                    "(ID INT PRIMARY KEY NOT NULL,"+
                    "NAME TEXT NOT NULL,"+
                    "AGE INT NOT NULL,"+
                    "ADDRESS CHAR(50),"+
                    "SALARY REAL)";
            stmt.executeUpdate(sql);
            stmt.close();
            conn.close();
                    

        } catch (ClassNotFoundException ex) {
            System.out.println("No library org.sqlite.JDBC");
            System.exit(0);
        } catch (SQLException ex) {
            System.out.println("Unable to connect database!!!");
            System.exit(0);
        }
        
        
    }
}
